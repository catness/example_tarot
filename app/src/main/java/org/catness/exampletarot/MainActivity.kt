package org.catness.exampletarot

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Display
import android.widget.Button
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.roundToInt

const val numImages = 3

class MainActivity : AppCompatActivity() {
    /* from the docs:
    "The lateinit keyword promises the Kotlin compiler that the variable will be initialized
    before the code calls any operations on it. Therefore we don't need to initialize the variable
    to null here, and we can treat it as a non-nullable variable when we use it."
    */
    /*
    lateinit var image1: ImageView
    lateinit var image2: ImageView
    lateinit var image3: ImageView
    */
    // Instead of 3 different variables, we can use an array of widgets:
    // https://stackoverflow.com/questions/55301944/array-of-buttons-in-kotlin
    lateinit var imageViews : Array<ImageView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Get the Button view from the layout and assign a click
        // listener to it.
        val rollButton: Button = findViewById(R.id.roll_button)
        rollButton.setOnClickListener { rollDice() }
        //image1 = findViewById(R.id.image1)
        //image2 = findViewById(R.id.image2)
        //image3 = findViewById(R.id.image3)
        imageViews = arrayOf(image1,image2,image3)
    }

    private fun rollDice() {
        // pick 3 random numbers with no duplicates
        // https://stackoverflow.com/questions/55212248/kotlin-a-list-of-random-distinct-numbers
        val s: MutableSet<Int> = mutableSetOf()
        while (s.size < 3) { s.add((0..77).random()) }
        val randomList = s.toList()

        for (i in 0..numImages-1) {
            var num = randomList[i] 
            // generate the name of imageResource
            var uri: String = "@drawable/tarot"+ num.toString()  // where myresource (without the extension) is the file
            // I don't remember where I got this method, but it works
            var imageResource: Int = applicationContext. getResources ().getIdentifier(uri, null, applicationContext.getPackageName());
            imageViews[i].setImageResource(imageResource)
            // show the Tarot card name on a tooltip, because it's often hard to understand from the image
            // tooltips require minSdkVersion 26
            imageViews[i].tooltipText = Tarot[num]
        }
    }

}
