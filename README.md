# About the project

**ExampleTarot** is an example for [Android Kotlin Fundamentals: Image resources and compatibility](https://codelabs.developers.google.com/codelabs/kotlin-android-training-images-compat) codelab, but with 3 Tarot cards instead of 2 dice.

Tarot images are taken from [Colman-Smith public domain deck](https://www.m31.de/colman-smith/index.html), and the back side of the card from [Wikipedia: Swiss card deck](https://commons.wikimedia.org/wiki/File:Swiss_card_deck_-_1850_-_Back_side.jpg).

The apk is here: [example-tarot.apk](apk/example-tarot.apk).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
